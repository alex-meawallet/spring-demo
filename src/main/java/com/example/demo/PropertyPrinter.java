package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PropertyPrinter {
    private static final Logger log = LoggerFactory.getLogger(PropertyPrinter.class);

    @Value("${mtp.test.param:0}")
    private int property;

    public void print() {
        log.info("Property value: {}", property);
    }
}
