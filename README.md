# Spring Boot demo

## Spring profiles

Which property value will be used based on profile selection.

### Setup 
Application properties contains following configuration per profile.
    
    mtp:
	  test:
	    param: 1
	---
	mtp:
	  test:
	    param: 2
	---
	spring:
	  profiles: test
	mtp:
	  test:
	    param: 3
	---
	spring:
	  profiles: prod
	mtp:
	  test:
	    param: 4
	---
	spring:
	  profiles: local

### Results
Results for running application with different profiles are as following.

|    Profile         |   Value    |          Output                                                              |    
|--------------------|------------|------------------------------------------------------------------------------|
|                    |`2`         | No active profile set, falling back to default profiles: default             |
|default             |`2`         | The following profiles are active: default                                   |
|local               |`2`         | The following profiles are active: local                                     |
|test                |`3`         | The following profiles are active: test                                      |
|prod                |`4`         | The following profiles are active: prod                                      |
|prod,local          |`4`         | The following profiles are active: prod,local                                |
|local,test          |`3`         | The following profiles are active: local,test                                |
|test,default        |`3`         | The following profiles are active: test,default                              |
|test,prod           |`4`         | The following profiles are active: test,prod                                 |
|prod,test           |`3`         | The following profiles are active: prod,test                                 |


